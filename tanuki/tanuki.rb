require 'gitlab'

PROJECT_ID = '24519238'

class Tanuki
    def client
        if @gl_client == nil 
            @gl_client = Gitlab.client(
                endpoint: 'https://gitlab.com/api/v4',
                private_token: ENV['GITLAB_TOKEN'],
            )
        else
            return @gl_client
        end
    end

    def pipeline_id
        return ENV['CI_PIPELINE_ID'] || 261417992
    end


    def get_job_trace(job_name)
        jobs = client.pipeline_jobs(PROJECT_ID, pipeline_id, per_page: 50)

        jobs.each { |job| 
            if job_name == job.name
                return client.job_trace(PROJECT_ID, job.id)
            end
        }

        return ''
    end
end
