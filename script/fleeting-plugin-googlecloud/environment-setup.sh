#! /usr/bin/bash

# Needed permissions:
#
# # for creating/deleting instance group
# compute.disks.create
# compute.instanceGroupManagers.create
# compute.instanceGroupManagers.delete
# compute.instanceGroups.create
# compute.instanceGroups.delete
# compute.instanceTemplates.get
# compute.instanceTemplates.useReadOnly
# compute.instances.create
# compute.instances.setMetadata
# compute.subnetworks.use
# compute.subnetworks.useExternalIp
#
# # for the plugin to operate with that instance group
#
# compute.instanceGroupManagers.get
# compute.instanceGroupManagers.update
# compute.instances.get
# compute.instances.setMetadata
#
# -------------------------------
#
# Needed environment variables:
#
# FLEETING_PLUGIN_GOOGLECLOUD_URL: The URL to download the latest release of the fleeting plugin
# GCP_PROJECT: Project to create instance group in
# GCP_ZONE: Zone to create instance group in
# GCP_INSTANCE_TEMPLATE_URL: Template for instance group to use
# GOOGLE_APPLICATION_CREDENTIALS: service account json for authentication
# GCP_INSTANCE_GROUP: Name of the instance group to create

# download plugin
curl -Lo /usr/local/bin/fleeting-plugin-googlecloud "${FLEETING_PLUGIN_GOOGLECLOUD_URL}"  && chmod +x /usr/local/bin/fleeting-plugin-googlecloud

# create runner config
cat << EOF > fleeting-plugin-googlecloud.toml
concurrent = 1
check_interval = 0
log_level = "debug"

[session_server]
  session_timeout = 1800

[[runners]]
  [runners.autoscaler]
    capacity_per_instance = 1
    max_use_count = 10
    max_instances = 1
    plugin = "fleeting-plugin-googlecloud"
    [runners.autoscaler.plugin_config]
      name = "${GCP_INSTANCE_GROUP}"
      project = "${GCP_PROJECT}"
      zone = "${GCP_ZONE}"
    [runners.autoscaler.connector_config]
      username = "fleeting"
      timeout = "10m"
      protocol = "ssh"
      use_external_addr = true
    [[runners.autoscaler.policy]]
      idle_count = 4
      idle_time  = "10m"
EOF