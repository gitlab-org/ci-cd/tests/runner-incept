#!/usr/bin/env sh

duration=$1
namespace=$2
interval=$3

end=$(expr $(date +%s) + "$duration")

echo "Start monitoring pods created in namespace '$namespace' for $duration seconds."

while true; do
    now=$(date +%s)
    if [ "$now" -ge "$end" ]; then
        break
    fi

    kubectl get pods -n "$namespace" -o wide

    sleep "$interval"
done

echo "End of the monitoring"
