param(
    [Parameter(Mandatory, Position = 0)]
    $Description,
    [Parameter(Mandatory, Position = 1)]
    $TagList
)

$headers = @{
    'PRIVATE-TOKEN' = "$Env:GITLAB_TOKEN_RUNNER_CREATION"
}

$body = @{
    'runner_type' = 'project_type'
    'project_id' = "$Env:CI_PROJECT_ID"
    'description' = "$Description"
    'access_level' = "not_protected"
    'locked' = 'true'
    'tag_list' = "$TagList"
}

$result = Invoke-RestMethod -Uri 'https://gitlab.com/api/v4/user/runners' -Method Post -Headers $headers -Body $body

return $result.token
