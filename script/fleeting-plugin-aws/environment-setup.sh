#! /usr/bin/bash

# The following environment variables are needed for the test to work
# They can be set at the runner-incept level as protected variables
# AWS_PROFILE: Name of the profile used by AWS to do the AWS things. Can be retrieved by creating a new IAM user 
# with the following policies
# -------------------------------
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "VisualEditor0",
#             "Effect": "Allow",
#             "Action": [
#                 "ec2:GetLaunchTemplateData",
#                 "autoscaling:SetDesiredCapacity",
#                 "ec2:RunInstances",
#                 "autoscaling:DeleteAutoScalingGroup",
#                 "autoscaling:TerminateInstanceInAutoScalingGroup",
#                 "autoscaling:CreateAutoScalingGroup"
#             ],
#             "Resource": [
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:instance/*",
#                 "arn:aws:resource-groups:AWS_REGION:AWS_ACCOUNT_ID:group/*",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:volume/*",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:key-pair/AWS_KEY_PAIR_NAME",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:network-interface/*",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:subnet/AWS_SUBNET_ID",
#                 "arn:aws:elastic-inference:AWS_REGION:AWS_ACCOUNT_ID:elastic-inference-accelerator/*",
#                 "arn:aws:license-manager:AWS_REGION:AWS_ACCOUNT_ID:license-configuration:*",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:placement-group/*",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:launch-template/AWS_LAUNCH_TEMPLATE_ID",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:elastic-gpu/*",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:capacity-reservation/*",
#                 "arn:aws:ec2:AWS_REGION::image/AWS_AMI_ID",
#                 "arn:aws:ec2:AWS_REGION:AWS_ACCOUNT_ID:security-group/AWS_SECURITY_GROUP_ID",
#                 "arn:aws:autoscaling:*:AWS_ACCOUNT_ID:autoScalingGroup:*:autoScalingGroupName/*"
#             ]
#         },
#         {
#             "Sid": "VisualEditor1",
#             "Effect": "Allow",
#             "Action": [
#                 "autoscaling:DescribeAutoScalingInstances",
#                 "ec2:DescribeInstances",
#                 "ec2:DescribeLaunchTemplates",
#                 "ec2:DescribeLaunchTemplateVersions"
#             ],
#             "Resource": "*"
#         },
#         {
#             "Sid": "VisualEditor2",
#             "Effect": "Allow",
#             "Action": "ec2:GetLaunchTemplateData",
#             "Resource": "arn:aws:ec2:*:AWS_ACCOUNT_ID:instance/*"
#         },
#         {
#             "Sid": "VisualEditor3",
#             "Effect": "Allow",
#             "Action": "ec2-instance-connect:SendSSHPublicKey",
#             "Resource": "arn:aws:ec2:*:AWS_ACCOUNT_ID:instance/*"
#         }
#     ]
# }
# -------------------------------
# FLEETING_PLUGIN_AWS_URL: The URL to download the latest release of the fleeting plugin AWS
# AWS_PROFILE: The AWS username associated to the  AWS Access Key
# AWS_LAUNCH_TEMPLATE_ID: ID of the launch template previously created and available in AWS
# AWS_REGION: The AWS region where the resources are available
# AWS_PROJECT: The AWS project (or account name) where the resources are hosted
# AWS_ACCESS_KEY_FILE: The path to the AWS Access Key
# AWS_INSTANCE_USERNAME: The default user to ssh on the instance. For ubuntu images is it "ubuntu", for Amazon Image: "ec2-user"
aws configure import --csv file://${AWS_ACCESS_KEY_FILE}

# Download latest version of the fleeting-plugin-aws for its package registry
curl -Lo /usr/local/bin/fleeting-plugin-aws "${FLEETING_PLUGIN_AWS_URL}"  && chmod +x /usr/local/bin/fleeting-plugin-aws

cat << EOF > fleeting-plugin-aws.toml
concurrent = 1
check_interval = 0
log_level = "debug"

[session_server]
  session_timeout = 1800

[[runners]]
  [runners.autoscaler]
    capacity_per_instance = 1
    max_use_count = 10
    max_instances = 1
    plugin = "fleeting-plugin-aws"
    [runners.autoscaler.plugin_config]
      credentials_profile = "${AWS_PROFILE}"
      name = "${AWS_AUTO_SCALING_GROUP}"
      project = "${AWS_PROJECT}"
      region = "${AWS_REGION}"
    [runners.autoscaler.connector_config]
      username = "${AWS_INSTANCE_USERNAME}"
      timeout = 600000000000
      protocol = "ssh"
      key_path = ""
      use_static_credentials = false
      use_external_addr = true
    [[runners.autoscaler.policy]]
      idle_count = 4
      idle_time  = 600000000000
      scale_factor = 0.0
      scale_factor_limit = 0
EOF