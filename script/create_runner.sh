#!/usr/bin/env sh

if [ $# -ne 2 ]; then
    echo "Runner name or Tag list not provided"
    exit 1
fi

result=$(curl "https://gitlab.com/api/v4/user/runners" \
        --silent \
        --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_RUNNER_CREATION}" \
        --data "runner_type=project_type" \
        --data "project_id=${CI_PROJECT_ID}" \
        --data "description=$1" \
        --data "access_level=not_protected" \
        --data "locked=true" \
        --data "tag_list=$2")

echo "$result" | jq -r ".token"
